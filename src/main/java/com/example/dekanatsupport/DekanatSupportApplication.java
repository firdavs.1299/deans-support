package com.example.dekanatsupport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DekanatSupportApplication {

    public static void main(String[] args) {
        SpringApplication.run(DekanatSupportApplication.class, args);
    }

}
